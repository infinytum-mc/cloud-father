package main

import (
	"time"

	"github.com/sirupsen/logrus"

	infinyteApi "bitbucket.org/infinyte/api"
	"bitbucket.org/infinytum-mc/cloud-father/api/authorization"
	"bitbucket.org/infinytum-mc/cloud-father/api/maps"

	"github.com/asaskevich/EventBus"
)

var (
	bus EventBus.Bus
	api *infinyteApi.APIServer
)

func main() {

	/*bus = EventBus.New()
	bus.Publish("ServerID:Server:Create", config.Server{Host: "localhost", ServerTypeId: 1})
	bus.Publish("ServerID:Server:Type:Info", config.ServerType{Id: 1, Name: "Quake", Maps: []string{"QuakeMap1"}, MaxPlayer: 20, Plugins: []string{"Quake.jar", "GameAPI.jar"}})
	bus.Publish("Proxy:Server:Register", config.McServer{Host: "localhost", Name: "Quake-1", Port: 4321})
	bus.Publish("Proxy:Server:Unregister", "Quake-1")
	bus.Subscribe("ServerID:ServerStats", serverHandler.StatHandler)*/

	api = infinyteApi.GetOrCreateAPI()
	api.Address = "0.0.0.0"
	api.Port = 8080
	api.Secret = "changeme"
	api.TokenValidity = time.Minute * 30
	api.Issuer = "CloudFather"
	api.LoginType = authorization.GetOrCreateLoginField(api.APIServerArguments)

	logrus.SetLevel(logrus.DebugLevel)

	api.RegisterType(&maps.Map{})

	api.ListenAndServe()
}
