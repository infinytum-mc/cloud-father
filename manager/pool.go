package manager

import "bitbucket.org/infinytum-mc/cloud-grandfather/config"

// PoolManager saves all registered server
type PoolManager struct {
	servers map[string]config.Server
}

func (pm PoolManager) addMcServer(server config.Server) {
	pm.servers[server.Name] = server
}

func (pm PoolManager) getMcServer(name string) (server config.Server) {
	return pm.servers[name]
}

func (pm PoolManager) removeMcServer(name string) {
	delete(pm.servers, name)
}
