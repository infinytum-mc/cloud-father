package authorization

import (
	"errors"

	"bitbucket.org/infinyte/api/args"

	"bitbucket.org/infinyte/api/jwt"
	"github.com/graphql-go/graphql"
	log "github.com/sirupsen/logrus"
)

var (
	loginField *graphql.Field
)

type Login struct {
	Token  string
	Ticket string
}

func GetOrCreateLoginField(args args.APIServerArguments) *graphql.Field {

	if loginField == nil {
		loginField = &graphql.Field{
			Type: graphql.NewObject(
				graphql.ObjectConfig{
					Name:        "Login",
					Description: "Authenticate and receive a token/ticket pair",

					Fields: graphql.Fields{
						"token": &graphql.Field{
							Description: "Freshly created token",
							Type:        graphql.String,
							Resolve: func(params graphql.ResolveParams) (interface{}, error) {
								refresh, ok := params.Source.(*Login)

								if !ok || refresh == nil {
									return nil, errors.New("Internal Server error while refreshing token")
								}

								return refresh.Token, nil
							},
						},
						"ticket": &graphql.Field{
							Description: "Matching refresh ticket to created token",
							Type:        graphql.String,
							Resolve: func(params graphql.ResolveParams) (interface{}, error) {
								refresh, ok := params.Source.(*Login)

								if !ok || refresh == nil {
									return nil, errors.New("Internal Server error while refreshing token")
								}

								return refresh.Ticket, nil
							},
						},
					},
				},
			),
			Description: "Authenticate against API",
			Args: graphql.FieldConfigArgument{
				"username": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				username, _ := params.Args["username"].(string)
				//password, _ := params.Args["password"].(string)

				claims := jwt.NewClaim(username, args.Issuer, args.TokenValidity, []string{"map", "map.location"})
				token, err := jwt.NewToken(claims, args.Secret)

				if err != nil {
					log.WithFields(log.Fields{"token": token.Plain}).Error("Could not create and sign new token for cloning process")
					return nil, errors.New("Internal server error while creating token")
				}

				return &Login{Token: token.Plain, Ticket: token.Ticket.Ticket}, nil
			},
		}
	}
	return loginField
}
