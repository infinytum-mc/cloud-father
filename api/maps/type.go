package maps

import (
	"bitbucket.org/infinyte/api"
	"github.com/graphql-go/graphql"
)

// Map is a GraphQL model class
type Map struct {
	api.APIType
	ID          string `permission:"map"`
	Name        string `permission:"map"`
	Description string `permission:"map"`
	Location    string `permission:"map.location"`
}

// Type returns the name of this model
func (t Map) Type() string {
	return "map"
}

// Data currently returns test data
func (t Map) Data(params graphql.ResolveParams) []interface{} {
	mySlice2 := make([]interface{}, 0)
	mySlice2 = append(mySlice2, Map{
		ID:   "Test",
		Name: "Yeah sure",
	})

	return mySlice2
}

func (t Map) QueryType() (graphql.ObjectConfig, graphql.FieldConfigArgument) {
	return queryType()
}
