package maps

import (
	"bitbucket.org/infinyte/api"
	"github.com/graphql-go/graphql"
)

func queryType() (graphql.ObjectConfig, graphql.FieldConfigArgument) {
	return graphql.ObjectConfig{
			Name:        "Map",
			Description: "Minecraft Server Map",
			Fields: graphql.Fields{
				"id": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "The unique map identifier",
					Resolve:     api.DefaultResolver("ID"),
				},
				"name": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "Human-readable name of the map",
					Resolve:     api.DefaultResolver("Name"),
				},
				"description": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "Human-readable description of the map",
					Resolve:     api.DefaultResolver("Description"),
				},
				"location": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.String),
					Description: "Machine-readable download location",
					Resolve:     api.DefaultResolver("Location"),
				},
			},
		},
		graphql.FieldConfigArgument{}
}
