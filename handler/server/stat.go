package server

import (
	"bitbucket.org/infinytum-mc/cloud-grandfather/config"

	log "github.com/sirupsen/logrus"
)

// StatHandler handels the stats of the Agents
func StatHandler(stats config.Memory) {
	log.WithFields(
		log.Fields{
			"cores":        stats.Cores,
			"used_memory":  stats.UsedRAM,
			"total_memory": stats.TotalRAM,
		},
	).Debug("Received server stats")
	//TODO
}
